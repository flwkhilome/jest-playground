const express = require('express');

const app = express();

app.get('/dolapo', (req, res) =>
  res.json({
    message: 'hi from dolapo',
    success: true,
  }),
);

app.get('/chinwendu', (req, res) =>
  res.json({
    message: 'hi from chinwendu',
    success: true,
  }),
);

module.exports = app;
