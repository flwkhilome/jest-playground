const http = require('supertest');
const app = require('./app');

describe('app test', () => {
  it('should respond with dolapo', async () => {
    const { body: response } = await http(app).get('/dolapo');
    expect(response).toBeDefined();
    expect(response).toHaveProperty('message');
    expect(response.message).toBe('hi from dolapo');
  });

  it('should respond with chinwendu', async () => {
    const { body: response } = await http(app).get('/chinwendu');
    expect(response).toBeDefined();
    expect(response).toHaveProperty('message');
    expect(response.message).toBe('hi from chinwendu');
  });
});
