const service = require('./service');

jest.mock('./db', () => {
  return {
    findByPk: jest.fn().mockReturnValueOnce({ name: 'john', age: 'real age' }),
  };
});

describe('test service', () => {
  it('should sha work', async () => {
    const res = await service({ person: 'john' });

    expect(res).toHaveProperty('name');
    expect(res).toHaveProperty('age');
    expect(res.name).toBe('john');
    expect(res.age).toBe('real age');
  });
});
