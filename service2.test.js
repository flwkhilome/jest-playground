const service2 = require('./service2');

jest.mock('./db', () => {
  return {
    monitor: 'hello',
  };
});

describe('test service', () => {
  it('should sha work', async () => {
    const res = await service2();
    expect(res).toBe('hello');
  });
});
